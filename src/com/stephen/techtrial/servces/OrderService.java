package com.stephen.techtrial.servces;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;

import com.owlike.genson.Genson;
import com.stephen.techtrial.resources.Customer;
import com.stephen.techtrial.resources.OrderLines;
import com.stephen.techtrial.resources.Product;
import com.stephen.techtrial.resources.SalesOrder;
import com.stephen.techtrial.util.DBActions;
import com.stephen.techtrial.util.HibernateUtil;
import com.stephen.techtrial.util.Utils;

@Path("order")
@Produces(MediaType.APPLICATION_JSON)
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
public class OrderService {
	private Utils utils = new Utils();
	private final static String url = "jdbc:mysql://localhost:3306/sales_order_backend";
	private static SessionFactory factory = HibernateUtil.getSessionJavaConfigFactory(url);

	private DBActions<OrderLines, String> dba = utils.new OrderServiceDBAction(OrderLines.class);
	private DBActions<Customer, String> dbCustomer = utils.new CustomerDBAction(Customer.class);
	private DBActions<Product, String> dbProduct = utils.new ProductDBAction(Product.class);
	private DBActions<SalesOrder, String> dbSales = utils.new SalesDBAction(SalesOrder.class);

	private Genson gen = new Genson();

	@GET
	@Path("{id}")
	public Response getOrder(@PathParam("id") String code) {
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		Session session = factory.getCurrentSession();
		dbTransaction = session.getTransaction();
		dbTransaction.begin();
		try {
			response = dbSales.findById(code);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;

	}

	@GET
	@Path("customer/{customerId}")
	public Response getOrdersByCustomer(@QueryParam("json") final String json,
			@PathParam("customerId") String customerId) {
		int offset = 0;
		int batch = 50;
		boolean ascending = true;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(SalesOrder.class);
			Customer cust = dbCustomer.find(customerId);
			if (cust == null) {
				return Response
						.status(Status.NOT_FOUND)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
										"There's no customer like that.")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			cr.add(Restrictions.eq("customer", cust));
			if (ascending) {
				cr.addOrder(Order.asc("code"));
			} else {
				cr.addOrder(Order.desc("code"));
			}
			cr.setFirstResult(offset);
			cr.setMaxResults(batch);
			response = dba.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;

	}

	@GET
	public Response getOrders(@QueryParam("json") final String json) {
		int offset = 0;
		int batch = 50;
		boolean ascending = true;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(SalesOrder.class);
			if (ascending) {
				cr.addOrder(Order.asc("orderNumber"));
			} else {
				cr.addOrder(Order.desc("orderNumber"));
			}
			cr.setFirstResult(offset);
			cr.setMaxResults(batch);
			response = dbSales.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@GET
	@Path("{salesOrderId}/line")
	public Response getOrdersLine(@QueryParam("json") final String json, @PathParam("salesOrderId") String salesOrderId) {
		int offset = 0;
		int batch = 50;
		boolean ascending = true;
		SalesOrder salesOrder = null;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(OrderLines.class);
			if (ascending) {
				cr.addOrder(Order.asc("orderNumberId"));
			} else {
				cr.addOrder(Order.desc("orderNumberId"));
			}
			salesOrder = dbSales.find(salesOrderId);
			cr.add(Restrictions.eq("orderNumberId", salesOrder));
			cr.setFirstResult(offset);
			cr.setMaxResults(batch);
			response = dbSales.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@GET
	@Path("{salesOrderId}/line/{productId}")
	public Response getOrderLineByProduct(@PathParam("productId") final String productId,
			@PathParam("salesOrderId") String salesOrderId) {

		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		SalesOrder salesOrder = null;
		Product product = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(OrderLines.class);

			salesOrder = dbSales.find(salesOrderId);
			cr.add(Restrictions.eq("orderNumberId", salesOrder));

			product = dbProduct.find(productId);
			cr.add(Restrictions.eq("productCode", product));
			response = dbSales.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@POST
	@Path("{customerId}")
	@Transactional
	public Response addOrder(@FormParam("json") final String json, @PathParam("customerId") String customerId) {
		Customer cust = null;
		Product product = null;
		OrderLines orderLine = null;
		SalesOrder salesOrder = null;
		BigDecimal totalSales = new BigDecimal(0.00);
		int quantity = 0;
		int originalQuantity = 0;
		BigDecimal previousPrice = new BigDecimal(0.00);
		BigDecimal unitPrice = new BigDecimal(0.00);
		BigDecimal totalPrice = new BigDecimal(0.00);
		String salesOrderNumber = null;
		String productCode = null;

		Session session = factory.getCurrentSession();
		Transaction dbTransaction = session.getTransaction();
		dbTransaction.begin();
		Criteria crSales = session.createCriteria(SalesOrder.class);
		try {
			JSONObject object = new JSONObject(json);
			cust = dbCustomer.find(customerId);
			if (cust == null) {

				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"That customer does ot exist")) + "}").cacheControl(utils.cacheControl())
						.build();
			}

			crSales.add(Restrictions.eq("code", customerId));
			salesOrderNumber = object.getString("salesOrderNumber");
			salesOrder = dbSales.find(salesOrderNumber);

			if (salesOrder == null) {
				salesOrder = new SalesOrder(salesOrderNumber, cust, totalSales, null);
				factory.getCurrentSession().save(salesOrder);
			}
			productCode = object.getString("productCode");
			product = dbProduct.find(productCode);
			if (product == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.NOT_FOUND)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
										"There's no product like that.")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			Criteria crOrderLine = session.createCriteria(OrderLines.class);
			crOrderLine.add(Restrictions.eq("orderNumberId", salesOrder));
			crOrderLine.add(Restrictions.eq("productCode", product));
			List<OrderLines> oList = dba.findByCriteria(crOrderLine);
			if (oList != null && !oList.isEmpty()) {

				orderLine = oList.get(0);
				previousPrice = orderLine.getTotalPrice();
				originalQuantity = orderLine.getQuantity();
			}
			quantity = object.getInt("quantity");
			if (quantity < 0) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but you cannot have a negative quantity.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}

			if (product.getQuantity() + originalQuantity < quantity) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but you can only buy a maximum of " + product.getQuantity()
												+ " items at the moment, until we get new stock")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			unitPrice = product.getPrice();
			totalPrice = unitPrice.multiply(new BigDecimal(quantity));

		} catch (Exception e) {
			e.printStackTrace();

			dbTransaction.rollback();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		String op = null;
		try {

			if (previousPrice.add(cust.getCurrentCredit().negate().add(cust.getCreditLimit())).compareTo(totalPrice) == 1) {

				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you have surpassed your credit limit.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			product.setQuantity(product.getQuantity() + originalQuantity - quantity);
			cust.setCurrentCredit(previousPrice.negate().add(cust.getCurrentCredit().add(totalPrice)));

			if (!salesOrder.getCustomer().equals(cust)) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils
										.createErrorResponse(Status.EXPECTATION_FAILED,
												"This sales order belongs to another customer, please use a different sales number"))
								+ "}").cacheControl(utils.cacheControl()).build();
			}
			OrderLines prevOrderLine = null;

			if (salesOrder.getOrderLines() == null || salesOrder.getOrderLines().isEmpty()) {
				Set<OrderLines> st = new HashSet<>();
				salesOrder.setOrderLines(st);
				if (orderLine == null) {
					orderLine = new OrderLines(String.valueOf(System.currentTimeMillis()), product, quantity,
							unitPrice, totalPrice, salesOrder);

				} else {
					prevOrderLine = orderLine;
					orderLine.setQuantity(quantity);
					orderLine.setTotalPrice(totalPrice);
				}
				session.save(orderLine);
				salesOrder.getOrderLines().add(orderLine);

			} else {
				if (orderLine == null) {
					orderLine = new OrderLines(String.valueOf(System.currentTimeMillis()), product, quantity,
							unitPrice, totalPrice, salesOrder);

				} else {
					prevOrderLine = orderLine;
					orderLine.setQuantity(quantity);
					orderLine.setTotalPrice(totalPrice);
				}
				session.save(orderLine);
				salesOrder.getOrderLines().remove(prevOrderLine);
				salesOrder.getOrderLines().add(orderLine);
			}

			salesOrder.setTotalPrice(previousPrice.negate().add(salesOrder.getTotalPrice().add(totalPrice)));
			session.update(salesOrder);
			cust.getSalesOrder().add(salesOrder);
			System.out.println("MADE");
			session.saveOrUpdate(cust);
			op = utils.om.writeValueAsString(cust);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if(dbTransaction!=null){
				dbTransaction.rollback();
			}
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again later.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return Response.ok(op).build();

	}

	@PUT
	@Path("{customerId}")
	@Transactional
	public Response editOrder(@FormParam("json") final String json, @PathParam("customerId") String customerId) {
		Customer cust = null;
		Product product = null;
		OrderLines orderLine = null;
		SalesOrder salesOrder = null;
		int quantity = 0;
		int originalQuantity = 0;
		BigDecimal previousPrice = new BigDecimal(0.00);
		BigDecimal unitPrice = new BigDecimal(0.00);
		BigDecimal totalPrice = new BigDecimal(0.00);
		String salesOrderNumber = null;
		String productCode = null;
		Session session = factory.getCurrentSession();
		Transaction dbTransaction = session.getTransaction();
		dbTransaction.begin();
		Criteria crSales = session.createCriteria(SalesOrder.class);
		String op = null;
		try {
			JSONObject object = new JSONObject(json);
			
			cust = dbCustomer.find(customerId);
			if (cust == null) {

				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"That customer does ot exist")) + "}").cacheControl(utils.cacheControl())
						.build();
			}

			crSales.add(Restrictions.eq("code", customerId));
			
			salesOrderNumber = object.getString("salesOrderNumber");
			salesOrder = dbSales.find(salesOrderNumber);
			System.out.println("OUT : 3");
			if (salesOrder == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you are editing a non existing sales order.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			productCode = object.getString("productCode");
			product = dbProduct.find(productCode);
			if (product == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.NOT_FOUND)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
										"There's no product like that.")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			Criteria crOrderLine = session.createCriteria(OrderLines.class);
			crOrderLine.add(Restrictions.eq("orderNumberId", salesOrder));
			crOrderLine.add(Restrictions.eq("productCode", product));
			List<OrderLines> oList = dba.findByCriteria(crOrderLine);
			if (oList == null || oList.isEmpty()) {
				return Response
						.status(Status.NOT_FOUND)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
										"There's no orderLine like that.")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			orderLine = oList.get(0);
			if (orderLine == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you are editing a non existing order line.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			previousPrice = orderLine.getTotalPrice();
			originalQuantity = orderLine.getQuantity();
			quantity = object.getInt("quantity");
			if (quantity < 0) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but you cannot have a negative quantity.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}

			System.out.println(product);
			if (product.getQuantity() + originalQuantity < quantity) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but you can only buy a maximum of " + product.getQuantity()
												+ " items at the moment, until we get new stock")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			unitPrice = product.getPrice();
			totalPrice = unitPrice.multiply(new BigDecimal(quantity));

		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		System.out.println("OUT : ");
		try {
			cust = dbCustomer.find(customerId);
			if (cust == null) {
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"That customer does not exist")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			System.out.println("OUT : 2");
			if (previousPrice.add(cust.getCurrentCredit().negate().add(cust.getCreditLimit())).compareTo(totalPrice) == 1) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you have surpassed your credit limit.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
			product.setQuantity(product.getQuantity() + originalQuantity - quantity);
			cust.setCurrentCredit(previousPrice.negate().add(cust.getCurrentCredit().add(totalPrice)));

			if (!salesOrder.getCustomer().equals(cust)) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils
										.createErrorResponse(Status.EXPECTATION_FAILED,
												"This sales order belongs to another customer, please use a different sales number"))
								+ "}").cacheControl(utils.cacheControl()).build();
			}
			OrderLines prevOrderLine = orderLine;
			salesOrder.getOrderLines().remove(prevOrderLine);
			orderLine.setQuantity(quantity);
			orderLine.setTotalPrice(totalPrice);
			session.update(orderLine);
			salesOrder.getOrderLines().add(orderLine);
			System.out.println("OUT : 4");

			salesOrder.setTotalPrice(previousPrice.negate().add(salesOrder.getTotalPrice().add(totalPrice)));
			cust.getSalesOrder().add(salesOrder);

			System.out.println("OUT : 5");
			session.saveOrUpdate(cust);
			op = utils.om.writeValueAsString(cust);
			dbTransaction.commit();
			System.out.println("OUT : 6");
		} catch (Exception e) {
			e.printStackTrace();
			if(dbTransaction!=null){
				dbTransaction.rollback();
			}
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again later.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return Response.ok(op).build();

	}

	@DELETE
	@Path("{salesOrderId}")
	public Response deleteSalesOrder(@PathParam("salesOrderId") String salesOrderId) {
		Customer cust = null;
		SalesOrder salesOrder = null;
		BigDecimal previousPrice = new BigDecimal(0.00);
		Session session = factory.getCurrentSession();
		Transaction dbTransaction = session.getTransaction();
		dbTransaction.begin();
		String op = null;
		try {
			salesOrder = dbSales.find(salesOrderId);
			System.out.println("OUT : 3");
			if (salesOrder == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you are deleting a non existing sales order.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
		
			for(OrderLines or:salesOrder.getOrderLines()){
				previousPrice.add(or.getTotalPrice());
				Product product=or.getProductCode();
				product.setQuantity(product.getQuantity()+or.getQuantity());
				session.saveOrUpdate(product);
				session.delete(or);
				
			}
			cust=salesOrder.getCustomer();
			cust.setCurrentCredit(previousPrice.negate().add(cust.getCurrentCredit()));
			
		
			cust.getSalesOrder().remove(salesOrder);
			session.saveOrUpdate(cust);
			
			session.delete(salesOrder);
			op = utils.om.writeValueAsString(salesOrder);
			dbTransaction.commit();
			System.out.println("OUT : 6");
		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again later.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return Response.ok(op).build();
	}
}

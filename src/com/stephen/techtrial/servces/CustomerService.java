package com.stephen.techtrial.servces;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.json.JSONObject;

import com.owlike.genson.Genson;
import com.stephen.techtrial.resources.Customer;
import com.stephen.techtrial.util.DBActions;
import com.stephen.techtrial.util.HibernateUtil;
import com.stephen.techtrial.util.Utils;

@Path("customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
public class CustomerService {
	private Utils utils = new Utils();
	private final static String url = "jdbc:mysql://localhost:3306/sales_order_backend";
	private static SessionFactory factory = HibernateUtil.getSessionJavaConfigFactory(url);
	private DBActions<Customer, String> dba = utils.new CustomerDBAction(Customer.class);

	private Genson gen = new Genson();

	@Path("test")
	@GET
	public Response activateUser() {
		return Response.ok("{\"test\":\"ok\"}").build();
	}

	@GET
	@Path("{id}")
	public Response getCustomer(@PathParam("id") String code) {
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		Session session = factory.getCurrentSession();
		dbTransaction = session.getTransaction();
		dbTransaction.begin();
		try {
			response = dba.findById(code);

			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;

	}

	@GET
	public Response getCustomers(@QueryParam("json") final String json) {
		int offset = 0;
		int batch = 50;
		boolean ascending = true;
		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}

		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(Customer.class);

			if (ascending) {
				cr.addOrder(Order.asc("code"));
			} else {
				cr.addOrder(Order.desc("code"));
			}
			cr.setFirstResult(offset);
			cr.setMaxResults(batch);
			response = dba.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;

	}

	@POST
	public Response addCustomer(@FormParam("json") final String json) {
		Customer cust = null;
		String code = null;
		String name = null;
		String address = null;
		String phone1 = null;
		String phone2 = null;
		BigDecimal creditLimit = null;
		BigDecimal currentCredit = new BigDecimal("0.00");
		try {
			JSONObject object = new JSONObject(json);
			code = object.getString("code");
			name = object.getString("name");
			address = object.getString("address");
			phone1 = object.getString("phone1");
			if (object.keySet().contains("phone2")) {
				phone2 = object.getString("phone2");
			}
			creditLimit = new BigDecimal(object.getString("creditLimit"));

		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		cust = new Customer(code, name, address, phone1, phone2, creditLimit, currentCredit);
		return dba.update(cust);

	}

	@PUT
	@Path("{id}")
	public Response editCustomer(@FormParam("json") final String json, @PathParam("id") String code) {
		Customer cust = null;
		String name = null;
		String address = null;
		String phone1 = null;
		String phone2 = null;
		BigDecimal creditLimit = null;
		try {
			JSONObject object = new JSONObject(json);
			name = object.getString("name");
			address = object.getString("address");
			phone1 = object.getString("phone1");
			if (object.keySet().contains("phone2")) {
				phone2 = object.getString("phone2");
			}
			creditLimit = new BigDecimal(object.getString("creditLimit"));

		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Transaction dbTransaction = null;
		Session session = factory.getCurrentSession();
		dbTransaction = session.getTransaction();
		dbTransaction.begin();
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"error\":"
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops it seems something went wrong please try again later")) + "}")
				.cacheControl(utils.cacheControl()).build();
		try {
			cust = dba.find(code);
			if (cust == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.NOT_FOUND)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
										"There's no customer like that.")) + "}").cacheControl(utils.cacheControl())
						.build();
			}
			cust.setName(name);
			cust.setAddress(address);
			cust.setCreditLimit(creditLimit);
			cust.setPhone1(phone1);
			cust.setPhone2(phone2);
			response = dba.update(cust);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again later")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@DELETE
	@Path("{id}")
	public Response deleteCustomer(@PathParam("id") String code) {

		return dba.delete(code);

	}

}

package com.stephen.techtrial.servces;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.owlike.genson.Genson;
import com.stephen.techtrial.resources.Customer;
import com.stephen.techtrial.resources.OrderLines;
import com.stephen.techtrial.resources.Product;
import com.stephen.techtrial.resources.SalesOrder;
import com.stephen.techtrial.util.DBActions;
import com.stephen.techtrial.util.HibernateUtil;
import com.stephen.techtrial.util.Utils;

@Path("product")
@Produces(MediaType.APPLICATION_JSON)
@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_FORM_URLENCODED})
public class ProductService {
	private Utils utils = new Utils();
	private final static String url = "jdbc:mysql://localhost:3306/sales_order_backend";
	private static SessionFactory factory = HibernateUtil.getSessionJavaConfigFactory(url);
	private DBActions<Product, String> dba = utils.new ProductDBAction(Product.class);

	private Genson gen = new Genson();

	@GET
	@Path("{id}")
	public Response getProduct(@PathParam("id") String code) {
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();

		Transaction dbTransaction = null;
		Session session = factory.getCurrentSession();
		dbTransaction = session.getTransaction();
		dbTransaction.begin();
		try {
			response = dba.findById(code);
			dbTransaction.commit();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@GET
	public Response getProducts(@QueryParam("json") final String json) {
		int offset = 0;
		int batch = 50;
		boolean ascending = true;

		try {
			JSONObject object = new JSONObject(json);
			if (object.keySet().contains("offset")) {
				offset = object.getInt("offset");
			}
			if (object.keySet().contains("batch")) {
				batch = object.getInt("batch");
			}
			if (object.keySet().contains("ascending")) {
				ascending = object.getBoolean("ascending");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"entity\": [{ }],\"error\" : "
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops  it seems something went wrong please try again.")) + "}")
				.cacheControl(utils.cacheControl()).build();
		Transaction dbTransaction = null;
		try {
			dbTransaction = factory.getCurrentSession().getTransaction();

			dbTransaction.begin();
			Criteria cr = factory.getCurrentSession().createCriteria(Product.class);

			if (ascending) {
				cr.addOrder(Order.asc("code"));
			} else {
				cr.addOrder(Order.desc("code"));
			}
			cr.setFirstResult(offset);
			cr.setMaxResults(batch);
			response = dba.list(cr);
			dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			response = Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"entity\": [{ }],\"error\" : "
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops  it seems something went wrong please try again.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;
	}

	@POST
	public Response addProduct(@FormParam("json") final String json) {
		Product product = null;
		BigDecimal price = null;
		String description = null;
		String code = null;
		int quantity = 0;
		try {
			JSONObject object = new JSONObject(json);
			price = new BigDecimal(object.getString("price"));
			description = object.getString("description");
			code = object.getString("code");
			quantity = object.getInt("quantity");

		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		product = new Product(code, description, price, quantity);
		return dba.update(product);

	}

	@PUT
	@Path("{id}")
	public Response editProduct(@FormParam("json") final String json, @PathParam("id") String code) {
		Product product = null;
		BigDecimal price = null;
		String description = null;
		int quantity = 0;
		try {
			JSONObject object = new JSONObject(json);
			price = new BigDecimal(object.getString("price"));
			description = object.getString("description");
			quantity = object.getInt("quantity");

		} catch (Exception e) {
			e.printStackTrace();

			return Response
					.status(Status.BAD_REQUEST)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.BAD_REQUEST,
									"Invalid JSON string. " + e.getMessage())) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		Transaction dbTransaction = null;
		Session session = factory.getCurrentSession();
		dbTransaction = session.getTransaction();
		dbTransaction.begin();
		Response response = Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"error\":"
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops it seems something went wrong please try again later")) + "}")
				.cacheControl(utils.cacheControl()).build();
		try {
		product = dba.find(code);
		if(product==null){
			return Response
					.status(Status.NOT_FOUND)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.NOT_FOUND,
									"There's no product like that.")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		product.setDescription(description);
		product.setPrice(price);
		product.setQuantity(quantity);
		response= dba.update(product);
		dbTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (dbTransaction != null)
				dbTransaction.rollback();
			return Response
					.status(Status.INTERNAL_SERVER_ERROR)
					.entity("{\"error\":"
							+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
									"Oops it seems something went wrong please try again later")) + "}")
					.cacheControl(utils.cacheControl()).build();
		}
		return response;

	}

	@DELETE
	@Path("{id}")
	public Response deleteProduct(@PathParam("id") String code) {
		BigDecimal previousPrice = new BigDecimal(0.00);
		Session session = factory.getCurrentSession();
		Transaction dbTransaction = session.getTransaction();
		dbTransaction.begin();
		String op = null;
		Product product=null;
		try {
			product=dba.find(code);
			if (product == null) {
				dbTransaction.rollback();
				return Response
						.status(Status.EXPECTATION_FAILED)
						.entity("{\"error\":"
								+ gen.serialize(utils.createErrorResponse(Status.EXPECTATION_FAILED,
										"Sorry but it seems you are deleting a non existing sales order.")) + "}")
						.cacheControl(utils.cacheControl()).build();
			}
					for(OrderLines or:product.getSalesOrders()){
						SalesOrder salesOrder=or.getOrderNumberId();
						if(salesOrder!=null){
							Customer cust=salesOrder.getCustomer();
					if(cust!=null){
						cust.setCurrentCredit(cust.getCurrentCredit().negate().add(salesOrder.getTotalPrice()));
					}
							session.saveOrUpdate(cust);
							for(OrderLines oro:salesOrder.getOrderLines()){
								session.delete(oro);
							}
							session.delete(salesOrder);
						}
					}
		session.delete(product);
		op = utils.om.writeValueAsString(product);
		dbTransaction.commit();
		System.out.println("OUT : 6");
	} catch (Exception e) {
		e.printStackTrace();
		return Response
				.status(Status.INTERNAL_SERVER_ERROR)
				.entity("{\"error\":"
						+ gen.serialize(utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
								"Oops it seems something went wrong please try again later.")) + "}")
				.cacheControl(utils.cacheControl()).build();
	}
	return Response.ok(op).build();

	}
}

package com.stephen.techtrial.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.owlike.genson.Genson;

public class DBActions<T, ID extends Serializable> {
	private Class<T> persistentClass;
	private SessionFactory factory = HibernateUtil.getSessionJavaConfigFactory(Utils.url);
	private Utils utils = new Utils();
	private Genson gen = new Genson();
	private Map<String, Object> res = new HashMap<String, Object>();

	public DBActions(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	protected Session getSession() {
		return factory.getCurrentSession();

	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	public Response add(T object) {
		Session session = getSession();
		Transaction tx = null;
		String id = null;
		try {
			tx = session.beginTransaction();
			id = (String) session.save(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			res.put("entity", object);
			res.put("error", utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops it seems something went wrong please try again. Maybe with a different code value"));
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(gen.serialize(res)).build();

		}
		if (id == null) {
			res.put("error", utils.createErrorResponse(Status.NOT_FOUND,
					"it seems there is nothing in the database, please add something."));
			res.put("entity", null);
			return Response.status(Status.NOT_FOUND).entity(gen.serialize(res)).build();
		} else {
			res.put("entity", object);
			return Response.ok(gen.serialize(res)).build();
		}
	}

	@SuppressWarnings("unchecked")
	public Response list(Criteria cr) throws JsonProcessingException {
		List<T> objects = null;

		objects = cr.list();

		System.out.println(objects);
		if (objects == null || objects.isEmpty()) {
			res.put("error",
					utils.createErrorResponse(Status.NOT_FOUND, "it seems there are no customers in the database"));
			res.put("entity", null);

			return Response.status(Status.NOT_FOUND).entity(gen.serialize(res)).build();
		} else {
			res.put("entity", objects);
			return Response.ok(utils.om.writeValueAsString(res)).build();
		}
	}

	@SuppressWarnings("unchecked")
	public Response delete(String id) {
		Session session = getSession();
		Transaction tx = null;
		T object = null;
		String ob=null;
		try {
			tx = session.beginTransaction();
			object = (T) session.get(getPersistentClass(), id);
			if(object==null){
				res.put("error", utils.createErrorResponse(Status.NOT_FOUND,
						"the database does not have it"));
				res.put("entity", null);
				return Response.status(Status.NOT_FOUND).entity(gen.serialize(res)).build();
			}
			session.delete(object);
			ob=utils.om.writeValueAsString(res);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			res.put("error", utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops it seems something went wrong please try again."));
			res.put("entity", null);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(gen.serialize(res)).build();
		} catch (JsonProcessingException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			res.put("error", utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops it seems something went wrong please try again."));
			res.put("entity", null);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(gen.serialize(res)).build();
		
		}
		
			res.put("entity", ob);
			return Response.ok(gen.serialize(res)).build();
		
	}

	@SuppressWarnings("unchecked")
	public Response findById(ID id) throws JsonProcessingException {

		T object = null;
		Session session = getSession();
		object = (T) session.get(getPersistentClass(), id);
		System.out.println(object);
		if (object == null) {
			res.put("error", utils.createErrorResponse(Status.NOT_FOUND, "it seems there's nothing in the database"));
			res.put("entity", object);
			return Response.status(Status.NOT_FOUND).entity(utils.om.writeValueAsString(res)).build();
		} else {
			res.put("entity", object);
			String out = utils.om.writeValueAsString(res);

			return Response.ok(out).build();
		}
	}

	public Response update(T object) {
		String resp=null;
		Session session = getSession();
		Transaction tx = null;
		String ob=null;
		try {
			tx = session.beginTransaction();
		session.saveOrUpdate(object);
		res.put("entity", object);
		resp=utils.om.writeValueAsString(res);
		tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			res.put("entity", object);
			res.put("error", utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops it seems something went wrong please try again. Maybe with a different code value"));
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(gen.serialize(res)).build();

		} catch (JsonProcessingException e) {
			e.printStackTrace();
			res.put("entity", object);
			res.put("error", utils.createErrorResponse(Status.INTERNAL_SERVER_ERROR,
					"Oops it seems something went wrong please try again. Maybe with a different code value"));
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(gen.serialize(res)).build();

		}
		return Response.ok(resp).build();
	}

	public Response update(T object, Session session) {
		session.saveOrUpdate(object);
		res.put("entity", object);
		session = null;
		return Response.ok(gen.serialize(res)).build();
	}

	@SuppressWarnings("unchecked")
	public T find(ID id) {
		T object = null;
		Session session = getSession();
		object = (T) session.get(getPersistentClass(), id);
		return object;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(Criteria cr) {
		List<T> object = null;

		object = cr.list();

		return object;
	}
}

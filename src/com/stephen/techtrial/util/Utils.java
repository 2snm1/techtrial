package com.stephen.techtrial.util;

import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.stephen.techtrial.resources.Customer;
import com.stephen.techtrial.resources.OrderLines;
import com.stephen.techtrial.resources.Product;
import com.stephen.techtrial.resources.SalesOrder;

public class Utils {

	public final static String url = "jdbc:mysql://localhost:3306/sales_order_backend";
	public ObjectMapper om = new ObjectMapper().registerModule(new Hibernate4Module());

	/**
	 * creates a JSON string status error message to be delivered to the
	 * developer for debugging
	 *
	 * @param status
	 *            The HTML error status
	 * @param message
	 *            the message to be appended to the response
	 * @return a String object that is sent to the developer
	 */
	public Map<String, Object> createErrorResponse(Status status, String message) {
		Map<String, Object> response = new HashMap<String, Object>();
		String description = "";
		try {
			switch (status.getStatusCode()) {
			case 400:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1";
				break;
			case 403:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4";
				break;
			case 404:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5";
				break;
			case 405:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6";
				break;
			case 409:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.10";
				break;
			case 412:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.13";
				break;
			case 415:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.16";
				break;
			case 417:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.18";
				break;
			case 500:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.1";
				break;
			case 503:
				description = "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.4";
				break;
			default:
				description = "Undocumented error. Please contact the bunshin team for help.";
				break;
			}

			response.put("status", status.getStatusCode() + " - " + status.getReasonPhrase());
			response.put("error message", escapeString(message));
			response.put("description", new URL(description));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * used to escape JSON strings
	 *
	 * @param text
	 *            the String value to be escaped
	 * @return a String value with all the escape sequences removed
	 */
	public String escapeString(String text) {
		text = text.replace("\"", "\'");
		text = text.replace("\'", "\'");
		text = text.replace("\n", "\\n");
		text = text.replace("\r", "\r");
		text = text.replace("\t", "\t");
		text = text.replace("\\", "\\");

		return text;
	}

	public CacheControl cacheControl() {
		CacheControl cc = new CacheControl();
		cc.setMaxAge(259200);

		return cc;

	}

	public void createDB() {
		ScriptRunner runner = null;
		System.out.println("creating");
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
			runner = new ScriptRunner(conn, false, true);
			runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/import.sql")));

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}

	public class CustomerDBAction extends DBActions<Customer, String> {
		// private Class<T> persistentClass=null;
		public CustomerDBAction(Class<Customer> persistentClass) {
			super(persistentClass);
			// TODO Auto-generated constructor stub
		}

	}

	public class OrderServiceDBAction extends DBActions<OrderLines, String> {

		public OrderServiceDBAction(Class<OrderLines> persistentClass) {
			super(persistentClass);
			// TODO Auto-generated constructor stub
		}

	}

	public class ProductDBAction extends DBActions<Product, String> {

		public ProductDBAction(Class<Product> persistentClass) {
			super(persistentClass);
			// TODO Auto-generated constructor stub
		}

	}

	public class SalesDBAction extends DBActions<SalesOrder, String> {

		public SalesDBAction(Class<SalesOrder> persistentClass) {
			super(persistentClass);
			// TODO Auto-generated constructor stub
		}

	}
}

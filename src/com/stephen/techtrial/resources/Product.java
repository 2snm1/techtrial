package com.stephen.techtrial.resources;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;





@Entity
@Table(name = "product")
public class Product {
	@Id
	@Column(name = "product_code", nullable = false, unique = true, columnDefinition = "VARCHAR(45)")
	private String code = null;
	@Column(name = "description", columnDefinition = "VARCHAR(225)", nullable = true)
	private String description = null;
	@Column(name = "price", nullable = false, columnDefinition = "DECIMAL(10,2) default 0.00")
	private BigDecimal price;
	@Column(name = "quantity", nullable = false, columnDefinition = "INT default 0")
	private int quantity = 0;
	@OnDelete(action=OnDeleteAction.CASCADE)
	@JsonManagedReference
	@OneToMany(mappedBy = "productCode", fetch = FetchType.LAZY)
	private Set<OrderLines> orderLines = null;

	public Product() {

	}

	public Product(String code, String description, BigDecimal price, int quantity) {
		super();
		this.code = code;
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@JsonIgnore
	public Set<OrderLines> getSalesOrders() {
		return orderLines;
	}

	public void setSalesOrders(Set<OrderLines> orderLines) {
		this.orderLines = orderLines;
	}

	@Override
	public String toString() {
		return "Product [code=" + code + ", description=" + description + ", price=" + price + ", quantity=" + quantity
				+ ", orderLines=" + orderLines + "]";
	}

}

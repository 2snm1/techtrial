package com.stephen.techtrial.resources;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "sales_order")
public class SalesOrder {
	@Id
	@Column(name = "order_number_id", nullable = false, unique = true, columnDefinition = "VARCHAR(45)")
	private String orderNumber = null;
	// @JsonBackReference
	@ManyToOne
	@JoinColumn(name = "customer_code", nullable = false, columnDefinition = "VARCHAR(45)")
	private Customer customer = null;
	@Column(name = "total_price", columnDefinition = "DECIMAL(10,2) default 0.00")
	private BigDecimal totalPrice = null;
	@JsonManagedReference
	@OneToMany(mappedBy = "orderNumberId", fetch = FetchType.LAZY)
	@OnDelete(action=OnDeleteAction.CASCADE)
	private Set<OrderLines> orderLines = null;

	public SalesOrder() {

	}

	public SalesOrder(String orderNumber, Customer customer, BigDecimal totalPrice,Set<OrderLines> orderLines) {
		super();
		this.orderNumber = orderNumber;
		this.customer = customer;
		this.totalPrice = totalPrice;
		this.orderLines=orderLines;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	@JsonIgnore
	public Set<OrderLines> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(Set<OrderLines> orderLines) {
		this.orderLines = orderLines;
	}

}

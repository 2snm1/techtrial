package com.stephen.techtrial.resources;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_lines")
public class OrderLines {
	@Id
	@Column(name = "order_lines_id", nullable = false, unique = true, columnDefinition = "VARCHAR(45)")
	private String orderLinesId = null;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "product_code", nullable = false, columnDefinition = "VARCHAR(45)")
	private Product productCode = null;
	@Column(name = "quantity", columnDefinition = "INT")
	private int quantity = 0;
	@Column(name = "unit_price", columnDefinition = "DECIMAL(10,2)")
	private BigDecimal unitPrice = null;
	@Column(name = "total_price", columnDefinition = "DECIMAL(10,2)")
	private BigDecimal totalPrice = null;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "order_number_id", nullable = false, columnDefinition = "VARCHAR(45)")
	private SalesOrder orderNumberId = null;

	public OrderLines() {
	}

	public OrderLines(String orderLinesId, Product productCode, int quantity, BigDecimal unitPrice,
			BigDecimal totalPrice, SalesOrder orderNumberId) {
		super();
		this.orderLinesId = orderLinesId;
		this.productCode = productCode;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.totalPrice = totalPrice;
		this.orderNumberId = orderNumberId;
	}

	public String getOrderLinesId() {
		return orderLinesId;
	}

	public void setOrderLinesId(String orderLinesId) {
		this.orderLinesId = orderLinesId;
	}

	public Product getProductCode() {
		return productCode;
	}

	public void setProductCode(Product productCode) {
		this.productCode = productCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
@JsonIgnore
	public SalesOrder getOrderNumberId() {
		return orderNumberId;
	}

	public void setOrderNumberId(SalesOrder orderNumberId) {
		this.orderNumberId = orderNumberId;
	}

}

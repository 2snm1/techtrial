package com.stephen.techtrial.resources;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@Column(name = "customer_code", nullable = false, columnDefinition = "VARCHAR(45)", unique = true)
	private String code = null;
	@Column(name = "name", nullable = false, columnDefinition = "VARCHAR(125)")
	private String name = null;
	@Column(name = "address", nullable = false, columnDefinition = "VARCHAR(125)")
	private String address = null;
	@Column(name = "phone_1", nullable = false, columnDefinition = "VARCHAR(45)")
	private String phone1 = null;
	@Column(name = "phone_2", nullable = false, columnDefinition = "VARCHAR(45)")
	private String phone2 = null;
	@Column(name = "credit_limit", nullable = false, columnDefinition = "DECIMAL(10,2) default 0.00")
	private BigDecimal creditLimit = null;
	@Column(name = "current_credit", nullable = false, columnDefinition = "DECIMAL(10,2) default 0.00")
	private BigDecimal currentCredit = null;
	 @JsonManagedReference
	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	 @OnDelete(action=OnDeleteAction.CASCADE)
	private Set<SalesOrder> salesOrder = null;

	public Customer() {
	}

	public Customer(String code, String name, String address, String phone1, String phone2, BigDecimal creditLimit,
			BigDecimal currentCredit) {
		super();
		this.code = code;
		this.name = name;
		this.address = address;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.creditLimit = creditLimit;
		this.currentCredit = currentCredit;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public BigDecimal getCurrentCredit() {
		return currentCredit;
	}

	public void setCurrentCredit(BigDecimal currentCredit) {
		this.currentCredit = currentCredit;
	}
@JsonIgnore
	public Set<SalesOrder> getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(Set<SalesOrder> salesOrder) {
		this.salesOrder = salesOrder;
	}

}

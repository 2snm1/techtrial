package com.stephen.techtrial.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;



@ApplicationPath("store")
public class SalesOrderApplication extends ResourceConfig {
	public SalesOrderApplication() {
		packages("com.stephen.techtrial").register(JacksonJsonProvider.class).register(RolesAllowedDynamicFeature.class).register(LoggingFilter.class);
	}
}

package com.stephen.techtrial;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;

import com.stephen.techtrial.util.HibernateUtil;
import com.stephen.techtrial.util.Utils;



public class Initializer implements ServletContextListener{
	private static SessionFactory factory = null;
	private Utils utils=new Utils();
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		if(factory!=null){
			factory.close();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		utils.createDB();
		factory = HibernateUtil.getSessionJavaConfigFactory(Utils.url);
		System.out.println("done");
		
	}

}

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `sales_order_backend` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `sales_order_backend` ;

-- -----------------------------------------------------
-- Table `sales_order_backend`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sales_order_backend`.`product` (
  `product_code` VARCHAR(45) NOT NULL,
  `description` VARCHAR(225) NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`product_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sales_order_backend`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sales_order_backend`.`customer` (
  `customer_code` VARCHAR(45) NOT NULL,
  `name` VARCHAR(125) NOT NULL,
  `address` VARCHAR(125) NOT NULL,
  `phone_1` VARCHAR(45) NOT NULL,
  `phone_2` VARCHAR(45) NULL,
  `credit_limit` DECIMAL(10,2) NULL DEFAULT 0,
  `current_credit` DECIMAL(10,2) NULL DEFAULT 0,
  PRIMARY KEY (`customer_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sales_order_backend`.`sales_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sales_order_backend`.`sales_order` (
  `order_number_id` VARCHAR(45) NOT NULL,
  `customer_code` VARCHAR(45) NOT NULL,
  `total_price` DECIMAL(10,2) NULL,
  PRIMARY KEY (`order_number_id`),
  INDEX `fk_sales_order_customer1_idx` (`customer_code` ASC),
  CONSTRAINT `fk_sales_order_customer1`
    FOREIGN KEY (`customer_code`)
    REFERENCES `sales_order_backend`.`customer` (`customer_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sales_order_backend`.`order_lines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sales_order_backend`.`order_lines` (
  `order_lines_id` VARCHAR(45) NOT NULL,
  `product_code` VARCHAR(45) NOT NULL,
  `quantity` INT NOT NULL DEFAULT 0,
  `unit_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00,
  `total_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00,
  `order_number_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`order_lines_id`),
  INDEX `fk_order_lines_product1_idx` (`product_code` ASC),
  INDEX `fk_order_lines_sales_order1_idx` (`order_number_id` ASC),
  CONSTRAINT `fk_order_lines_product1`
    FOREIGN KEY (`product_code`)
    REFERENCES `sales_order_backend`.`product` (`product_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_lines_sales_order1`
    FOREIGN KEY (`order_number_id`)
    REFERENCES `sales_order_backend`.`sales_order` (`order_number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
